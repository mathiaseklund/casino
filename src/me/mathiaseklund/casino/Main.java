package me.mathiaseklund.casino;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	static Main main;
	Util util;
	Config config;
	Roulette roulette;
	Players players;

	public static Main getMain() {
		return main;
	}

	public void onEnable() {
		main = this;
		util = new Util();
		File f = new File(this.getDataFolder(), "config.yml");
		if (!f.exists()) {
			saveResource("config.yml", true);
		}

		config = new Config(f);

		if (config.getRoulette()) {
			Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
				public void run() {
					roulette = new Roulette();
				}
			});

		}

		players = new Players();

		loadCommands();
		loadListeners();
	}

	void loadCommands() {
		getCommand("roulette").setExecutor(new RouletteCommand());
	}

	void loadListeners() {
		getServer().getPluginManager().registerEvents(new PlayerListener(), this);
		if (getCfg().getRoulette()) {
			getServer().getPluginManager().registerEvents(new RouletteEvents(), this);
		}
	}

	public Util getUtil() {
		return util;
	}

	public Config getCfg() {
		return config;
	}

	public Roulette getRoulette() {
		return roulette;
	}

	public Players getPlayers() {
		return players;
	}
}
