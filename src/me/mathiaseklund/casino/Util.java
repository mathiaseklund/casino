package me.mathiaseklund.casino;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

import net.md_5.bungee.api.ChatColor;

public class Util {

	Main main;

	public Util() {
		main = Main.getMain();
	}

	public void debug(String message) {
		if (main.getCfg().getDebug()) {
			message(Bukkit.getConsoleSender(), "&7[&eDEBUG&7]&r " + message);
		}
	}

	public void message(CommandSender sender, String message) {
		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
	}

	public void error(CommandSender sender, String message) {
		message(sender, "&4ERROR:&7 " + message);
	}

	public void error(String message) {
		message(Bukkit.getConsoleSender(), message);
	}
}
