package me.mathiaseklund.casino;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.mysql.jdbc.Util;

public class Config {

	Main main;
	Util util;

	File f;
	FileConfiguration fc;

	boolean debug, roulette;

	public Config(File f) {
		main = Main.getMain();
		this.f = f;
		fc = YamlConfiguration.loadConfiguration(f);
		load();
	}

	void load() {
		debug = fc.getBoolean("debug");
		roulette = fc.getBoolean("roulette.enabled");
	}

	void save() {
		try {
			fc.save(f);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean getDebug() {
		return debug;
	}

	public void setDebug(boolean b) {
		debug = b;
		fc.set("debug", b);
		save();
	}

	public boolean getRoulette() {
		return roulette;
	}

	public void setRoulette(boolean b) {
		roulette = b;
		fc.set("roulette.enabled", b);
		save();
	}

	public FileConfiguration getConfig() {
		return fc;
	}
}
