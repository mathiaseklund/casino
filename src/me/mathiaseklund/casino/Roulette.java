package me.mathiaseklund.casino;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import net.md_5.bungee.api.ChatColor;

public class Roulette {

	Main main;
	Util util;

	File folder;

	HashMap<String, RouletteTable> tables;

	HashMap<String, String> viewingTable; // playerName, tableId

	public NamespacedKey greenKey, redKey, blackKey, rouletteKey, minbetKey, maxbetKey, raiseKey, lowerKey, currentKey,
			betTypeKey;

	public Roulette() {
		main = Main.getMain();
		util = main.getUtil();
		folder = new File(main.getDataFolder() + "/roulette/");
		if (!folder.exists()) {
			folder.mkdir();
			main.saveResource("roulette/general.yml", true);
		}

		load();
	}

	void load() {
		tables = new HashMap<String, RouletteTable>();

		greenKey = new NamespacedKey(main, "green");
		redKey = new NamespacedKey(main, "red");
		blackKey = new NamespacedKey(main, "black");
		rouletteKey = new NamespacedKey(main, "roulette");
		minbetKey = new NamespacedKey(main, "minbet");
		maxbetKey = new NamespacedKey(main, "maxbet");
		raiseKey = new NamespacedKey(main, "raise");
		lowerKey = new NamespacedKey(main, "lower");
		currentKey = new NamespacedKey(main, "currentbet");
		betTypeKey = new NamespacedKey(main, "bettype");

		for (File f : folder.listFiles()) {
			RouletteTable table = new RouletteTable(f, this);
			tables.put(f.getName().replace(".yml", ""), table);
		}

		viewingTable = new HashMap<String, String>();

	}

	public RouletteTable getRouletteTable(String key) {
		return tables.get(key);
	}

	public boolean doesRouletteTableExist(String key) {
		return tables.containsKey(key);
	}

	public void openRouletteTable(Player player, String key) {
		if (doesRouletteTableExist(key)) {
			RouletteTable table = getRouletteTable(key);
			viewingTable.put(player.getName(), key);
			addRouletteControls(player, table);
			Bukkit.getScheduler().runTask(main, new Runnable() {
				public void run() {
					player.openInventory(table.getTable());
				}
			});

		} else {
			util.error(player, "Unable to open requested table.");
		}
	}

	public boolean isViewingTable(Player player) {
		return viewingTable.containsKey(player.getName());
	}

	public void addRouletteControls(Player player, RouletteTable table) {
		player.getInventory().clear();

		Inventory inv = player.getInventory();
		addRaiseItems(inv, table);
		addLowerItems(inv, table);
		addCurrentBetItem(inv, table, player);
		addBetItems(inv, table, player);
	}

	public Inventory addRaiseItems(Inventory inv, RouletteTable table) {
		ConfigurationSection section = main.getCfg().getConfig().getConfigurationSection("roulette.controls.raise");
		String material = section.getString("material", "GREEN_STAINED_GLASS_PANE");
		String name = section.getString("name", "&a+%increment%");
		List<String> lore = section.getStringList("lore");
		List<Integer> slots = section.getIntegerList("slots");
		int c = 0;
		for (int slot : slots) {
			c++;
			double increment = table.minbet;
			if (c < 2) {
				// do nothing
			} else if (c == 2) {
				increment = increment * 10;
			} else if (c == 3) {
				increment = increment * 100;
			} else if (c == 4) {
				increment = (increment * 100) * 2;
			} else {
				increment = ((increment * 100) * 2) * 2;
			}
			ItemStack is = new ItemStack(Material.getMaterial(material.toUpperCase()));
			ItemMeta im = is.getItemMeta();
			im.setDisplayName(
					ChatColor.translateAlternateColorCodes('&', name.replaceAll("%increment%", increment + "")));
			List<String> list = new ArrayList<String>();
			for (String s : lore) {
				list.add(ChatColor.translateAlternateColorCodes('&', s.replace("%increment%", increment + "")));
			}
			im.setLore(list);

			PersistentDataContainer container = im.getPersistentDataContainer();
			container.set(rouletteKey, PersistentDataType.INTEGER, 1);
			container.set(raiseKey, PersistentDataType.DOUBLE, increment);

			is.setItemMeta(im);
			inv.setItem(slot, is);
		}
		return inv;
	}

	public Inventory addLowerItems(Inventory inv, RouletteTable table) {
		ConfigurationSection section = main.getCfg().getConfig().getConfigurationSection("roulette.controls.lower");
		String material = section.getString("material", "RED_STAINED_GLASS_PANE");
		String name = section.getString("name", "&c-%increment%");
		List<String> lore = section.getStringList("lore");
		List<Integer> slots = section.getIntegerList("slots");
		int c = 0;
		for (int slot : slots) {
			c++;
			double increment = table.minbet;
			if (c < 2) {
				// do nothing
			} else if (c == 2) {
				increment = increment * 10;
			} else if (c == 3) {
				increment = increment * 100;
			} else if (c == 4) {
				increment = (increment * 100) * 2;
			} else {
				increment = ((increment * 100) * 2) * 2;
			}
			ItemStack is = new ItemStack(Material.getMaterial(material.toUpperCase()));
			ItemMeta im = is.getItemMeta();
			im.setDisplayName(
					ChatColor.translateAlternateColorCodes('&', name.replaceAll("%increment%", increment + "")));
			List<String> list = new ArrayList<String>();
			for (String s : lore) {
				list.add(ChatColor.translateAlternateColorCodes('&', s.replace("%increment%", increment + "")));
			}
			im.setLore(list);

			PersistentDataContainer container = im.getPersistentDataContainer();
			container.set(rouletteKey, PersistentDataType.INTEGER, 1);
			container.set(lowerKey, PersistentDataType.DOUBLE, increment);

			is.setItemMeta(im);
			inv.setItem(slot, is);
		}
		return inv;
	}

	public Inventory addCurrentBetItem(Inventory inv, RouletteTable table, Player player) {
		ConfigurationSection section = main.getCfg().getConfig()
				.getConfigurationSection("roulette.controls.currentbet");
		String material = section.getString("material", "YELLOW_STAINED_GLASS_PANE");
		String name = section.getString("name", "&eSelected Bet: &6%selectedbet%");
		List<String> lore = section.getStringList("lore");
		int slot = section.getInt("slot");

		double selectedBet = table.getSelectedBet(player);

		ItemStack is = new ItemStack(Material.getMaterial(material.toUpperCase()));
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(ChatColor.translateAlternateColorCodes('&', name.replace("%selectedbet%", selectedBet + "")));
		List<String> list = new ArrayList<String>();
		for (String s : lore) {
			if (s.contains("%bet%")) {
				List<String> betlist = table.getBetList(player.getName());
				if (betlist != null) {
					if (!betlist.isEmpty()) {
						for (String bet : betlist) {
							String[] parts = bet.split(" ");
							String betType = parts[0];
							String betAmount = parts[1];

							list.add(ChatColor.translateAlternateColorCodes('&',
									s.replace("%bet%", betAmount).replace("%place%", betType.toUpperCase())));
						}
					} else {
						list.add(ChatColor.translateAlternateColorCodes('&', "&7- &4None"));
					}
				} else {
					list.add(ChatColor.translateAlternateColorCodes('&', "&7- &4None"));
				}
			} else {
				list.add(ChatColor.translateAlternateColorCodes('&', s));
			}
		}
		im.setLore(list);

		im.addItemFlags(ItemFlag.HIDE_ENCHANTS);

		PersistentDataContainer container = im.getPersistentDataContainer();
		container.set(rouletteKey, PersistentDataType.INTEGER, 1);
		container.set(currentKey, PersistentDataType.INTEGER, 1);

		is.setItemMeta(im);

		if (selectedBet > 0) {
			is.addUnsafeEnchantment(Enchantment.SILK_TOUCH, 1);
		}
		inv.setItem(slot, is);

		return inv;
	}

	public Inventory addBetItems(Inventory inv, RouletteTable table, Player player) {
		for (String type : table.getBetTypes()) {
			ConfigurationSection section = main.getCfg().getConfig()
					.getConfigurationSection("roulette.controls." + type);
			String material = section.getString("material", "RED_STAINED_GLASS_PANE");
			String name = section.getString("name", "&6Place Bet on &cRED");
			List<String> lore = section.getStringList("lore");
			int slot = section.getInt("slot");

			double selectedBet = table.getSelectedBet(player);

			ItemStack is = new ItemStack(Material.getMaterial(material.toUpperCase()));
			ItemMeta im = is.getItemMeta();
			im.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
			List<String> list = new ArrayList<String>();
			for (String s : lore) {
				s = s.replace("%selectedbet%", selectedBet + "");
				list.add(ChatColor.translateAlternateColorCodes('&', s));
			}
			im.setLore(list);

			im.addItemFlags(ItemFlag.HIDE_ENCHANTS);

			PersistentDataContainer container = im.getPersistentDataContainer();
			container.set(rouletteKey, PersistentDataType.INTEGER, 1);
			container.set(betTypeKey, PersistentDataType.STRING, type);

			is.setItemMeta(im);

			inv.setItem(slot, is);
		}
		return inv;

	}

	public RouletteTable getCurrentTable(Player player) {
		return getRouletteTable(viewingTable.get(player.getName()));
	}
}
