package me.mathiaseklund.casino;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class PlayerData {

	Main main;
	Util util;
	Player player;
	File f;
	FileConfiguration fc;
	boolean loading = true;

	double money;

	public PlayerData(Player player) {
		this.player = player;
		main = Main.getMain();
		util = main.getUtil();
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				load();
			}
		});
	}

	void load() {
		// load player data.
		f = new File(main.getDataFolder() + "/players/" + player.getUniqueId().toString() + ".yml");
		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		fc = YamlConfiguration.loadConfiguration(f);

		money = fc.getDouble("money", 100);

		loading = false;
	}

	void save() {
		try {
			fc.save(f);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean isLoading() {
		return loading;
	}

	public double getMoney() {
		return money;
	}

	public double setMoney(double d) {
		money = d;
		fc.set("money", d);
		save();
		return money;
	}

	public double addMoney(double d) {
		return setMoney(money + d);
	}

	public double takeMoney(double d) {
		return setMoney(money - d);
	}
}
