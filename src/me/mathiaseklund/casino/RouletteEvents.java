package me.mathiaseklund.casino;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

public class RouletteEvents implements Listener {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		if (!event.isCancelled()) {
			Player player = (Player) event.getWhoClicked();
			if (player != null) {
				Roulette roulette = main.getRoulette();
				if (roulette.isViewingTable(player)) {
					event.setCancelled(true);
					ItemStack is = event.getCurrentItem();
					Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
						public void run() {
							if (is != null) {
								ItemMeta im = is.getItemMeta();
								PersistentDataContainer container = im.getPersistentDataContainer();
								if (container.has(roulette.raiseKey, PersistentDataType.DOUBLE)) {
									double d = container.get(roulette.raiseKey, PersistentDataType.DOUBLE);
									util.debug("Raise Amount: " + d);
									RouletteTable table = roulette.getCurrentTable(player);
									table.setSelectedBet(player, table.getSelectedBet(player) + d);
									roulette.addCurrentBetItem(player.getInventory(), table, player);
								} else if (container.has(roulette.lowerKey, PersistentDataType.DOUBLE)) {
									double d = container.get(roulette.lowerKey, PersistentDataType.DOUBLE);
									util.debug("Lower Amount: " + d);
									RouletteTable table = roulette.getCurrentTable(player);
									table.setSelectedBet(player, table.getSelectedBet(player) - d);
									roulette.addCurrentBetItem(player.getInventory(), table, player);
								} else if (container.has(roulette.betTypeKey, PersistentDataType.STRING)) {
									String type = container.get(roulette.betTypeKey, PersistentDataType.STRING);
									util.debug("Bet Type: " + type);
									RouletteTable table = roulette.getCurrentTable(player);
									if (!table.isRolling()) {
										table.placeBet(player, type);
										roulette.addCurrentBetItem(player.getInventory(), table, player);
									} else {
										util.error(player, "Can't place bet while rolling.");
									}
								}
							}
						}
					});
				}
			}
		}
	}

}
