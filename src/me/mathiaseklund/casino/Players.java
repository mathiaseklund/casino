package me.mathiaseklund.casino;

import java.io.File;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Players {

	Main main;
	Util util;

	HashMap<String, PlayerData> players;

	public Players() {
		main = Main.getMain();
		util = main.getUtil();

		load();

	}

	void load() {

		File folder = new File(main.getDataFolder() + "/players/");
		if (!folder.exists()) {
			folder.mkdir();
		}

		players = new HashMap<String, PlayerData>();

		loadOnlinePlayers();
	}

	void loadOnlinePlayers() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			loadPlayer(p);
		}
	}

	public void loadPlayer(Player p) {
		PlayerData data = new PlayerData(p);
		players.put(p.getName(), data);
	}

	public PlayerData getPlayerData(Player player) {
		return getPlayerData(player.getName());
	}

	public PlayerData getPlayerData(String name) {
		return players.get(name);
	}
}
