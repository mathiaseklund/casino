package me.mathiaseklund.casino;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

public class RouletteTable {

	Main main;
	Util util;
	File f;
	FileConfiguration fc;
	Roulette roulette;

	String name, id;
	double minbet, maxbet;
	int speed, interval;
	boolean rolling = false;
	long rollTime;
	int rollingTime;
	long stopRolling;

	HashMap<String, Double> selectedBets = new HashMap<String, Double>();
	HashMap<String, List<String>> placedBets = new HashMap<String, List<String>>(); // player, betList
	List<String> bettypes;
	Inventory table;

	public RouletteTable(File f, Roulette roulette) {
		main = Main.getMain();
		util = main.getUtil();
		this.roulette = roulette;
		this.f = f;
		fc = YamlConfiguration.loadConfiguration(f);
		id = f.getName().replace(".yml", "");
		load();
	}

	void load() {
		name = fc.getString("name");
		minbet = fc.getDouble("minbet");
		maxbet = fc.getDouble("maxbet");
		speed = fc.getInt("speed", 2);
		interval = fc.getInt("interval", 7);
		rollingTime = fc.getInt("rollingtime", 5);

		bettypes = fc.getStringList("bettypes");

		createTable();
		updateTable();
	}

	void save() {
		try {
			fc.save(f);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void createTable() {
		table = Bukkit.createInventory(null, 45, ChatColor.translateAlternateColorCodes('&', name));
		boolean red = true;
		for (int i = 0; i < 37; i++) {
			if (i == 0) {
				// add green
				ItemStack is = new ItemStack(Material.GREEN_STAINED_GLASS_PANE);
				ItemMeta im = is.getItemMeta();
				im.setDisplayName(ChatColor.GREEN + "" + i);
				List<String> lore = new ArrayList<String>();
				lore.add(ChatColor.translateAlternateColorCodes('&', "&7Click to bet on 0 (&aGREEN&7)"));
				lore.add(ChatColor.translateAlternateColorCodes('&', "&eBet Win: &636x"));
				im.setLore(lore);

				PersistentDataContainer container = im.getPersistentDataContainer();
				container.set(roulette.greenKey, PersistentDataType.INTEGER, i);
				container.set(roulette.rouletteKey, PersistentDataType.INTEGER, 1);
				container.set(roulette.minbetKey, PersistentDataType.DOUBLE, minbet);
				container.set(roulette.maxbetKey, PersistentDataType.DOUBLE, maxbet);

				im.addItemFlags(ItemFlag.HIDE_ENCHANTS);

				is.setItemMeta(im);
				for (int s = 36; s < 45; s++) {
					table.setItem(s, is);
				}
			} else {
				if (red) {
					red = false;
					// add red
					ItemStack is = new ItemStack(Material.RED_STAINED_GLASS_PANE, i);
					ItemMeta im = is.getItemMeta();
					im.setDisplayName(ChatColor.RED + "" + i);
					List<String> lore = new ArrayList<String>();
					lore.add(ChatColor.translateAlternateColorCodes('&', "&7Click to bet on " + i + " (&cRED&7)"));
					lore.add(ChatColor.translateAlternateColorCodes('&', "&eBet Win: &636x"));
					im.setLore(lore);

					PersistentDataContainer container = im.getPersistentDataContainer();
					container.set(roulette.redKey, PersistentDataType.INTEGER, i);
					container.set(roulette.rouletteKey, PersistentDataType.INTEGER, 1);
					container.set(roulette.minbetKey, PersistentDataType.DOUBLE, minbet);
					container.set(roulette.maxbetKey, PersistentDataType.DOUBLE, maxbet);
					im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
					is.setItemMeta(im);
					table.setItem(i - 1, is);
				} else {
					red = true;
					// add black
					ItemStack is = new ItemStack(Material.BLACK_STAINED_GLASS_PANE, i);
					ItemMeta im = is.getItemMeta();
					im.setDisplayName(ChatColor.DARK_GRAY + "" + i);
					List<String> lore = new ArrayList<String>();
					lore.add(ChatColor.translateAlternateColorCodes('&', "&7Click to bet on " + i + " (&0BLACK&7)"));
					lore.add(ChatColor.translateAlternateColorCodes('&', "&eBet Win: &636x"));
					im.setLore(lore);

					PersistentDataContainer container = im.getPersistentDataContainer();
					container.set(roulette.blackKey, PersistentDataType.INTEGER, i);
					container.set(roulette.rouletteKey, PersistentDataType.INTEGER, 1);
					container.set(roulette.minbetKey, PersistentDataType.DOUBLE, minbet);
					container.set(roulette.maxbetKey, PersistentDataType.DOUBLE, maxbet);
					im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
					is.setItemMeta(im);
					table.setItem(i - 1, is);
				}
			}
		}

		rollTime = Calendar.getInstance().getTimeInMillis() + (interval * 1000);
	}

	void updateTable() {
		boolean red = true;
		long now = Calendar.getInstance().getTimeInMillis();
		long diff = 0;
		if (now < rollTime) {
			diff = rollTime - now;
			if (diff > 0) {
				diff = diff / 1000;
			} else {
				diff = 0;
			}
		}

		if (diff <= 0) {
			if (!rolling) {
				rolling = true;
				stopRolling = now + (rollingTime * 1000);
				for (int i = 0; i < 37; i++) {
					if (i == 0) {
						ItemStack is = table.getItem(44);
						is.removeEnchantment(Enchantment.SILK_TOUCH);
						for (int s = 36; s < 45; s++) {
							table.setItem(s, is);
						}
					} else {
						ItemStack is = table.getItem(i - 1);
						is.removeEnchantment(Enchantment.SILK_TOUCH);
						table.setItem(i - 1, is);
					}
				}
				rollTable(-1);
			}
		}

		for (int i = 0; i < 37; i++) {
			if (i == 0) {
				// add green
				ItemStack is = table.getItem(44);
				ItemMeta im = is.getItemMeta();
				im.setDisplayName(ChatColor.GREEN + "" + i);
				List<String> lore = new ArrayList<String>();
				lore.add(ChatColor.translateAlternateColorCodes('&', "&7Click to bet on 0 (&aGREEN&7)"));
				lore.add(ChatColor.translateAlternateColorCodes('&', "&eBet Win: &636x"));
				if (!rolling) {
					lore.add(ChatColor.translateAlternateColorCodes('&', "&eRolling in &a" + diff + " Seconds"));
				} else {
					lore.add(ChatColor.translateAlternateColorCodes('&', "&3&lROLLING"));
				}
				im.setLore(lore);

				PersistentDataContainer container = im.getPersistentDataContainer();
				container.set(roulette.greenKey, PersistentDataType.INTEGER, i);
				container.set(roulette.rouletteKey, PersistentDataType.INTEGER, 1);
				container.set(roulette.minbetKey, PersistentDataType.DOUBLE, minbet);
				container.set(roulette.maxbetKey, PersistentDataType.DOUBLE, maxbet);
				im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
				is.setItemMeta(im);
				for (int s = 36; s < 45; s++) {
					table.setItem(s, is);
				}
			} else {
				if (red) {
					red = false;
					// add red
					ItemStack is = table.getItem(i - 1);
					ItemMeta im = is.getItemMeta();
					im.setDisplayName(ChatColor.RED + "" + i);
					List<String> lore = new ArrayList<String>();
					lore.add(ChatColor.translateAlternateColorCodes('&', "&7Click to bet on " + i + " (&cRED&7)"));
					lore.add(ChatColor.translateAlternateColorCodes('&', "&eBet Win: &636x"));
					if (!rolling) {
						lore.add(ChatColor.translateAlternateColorCodes('&', "&eRolling in &a" + diff + " Seconds"));
					} else {
						lore.add(ChatColor.translateAlternateColorCodes('&', "&3&lROLLING"));
					}
					im.setLore(lore);

					PersistentDataContainer container = im.getPersistentDataContainer();
					container.set(roulette.redKey, PersistentDataType.INTEGER, i);
					container.set(roulette.rouletteKey, PersistentDataType.INTEGER, 1);
					container.set(roulette.minbetKey, PersistentDataType.DOUBLE, minbet);
					container.set(roulette.maxbetKey, PersistentDataType.DOUBLE, maxbet);
					im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
					is.setItemMeta(im);
					table.setItem(i - 1, is);
				} else {
					red = true;
					// add black
					ItemStack is = table.getItem(i - 1);
					ItemMeta im = is.getItemMeta();
					im.setDisplayName(ChatColor.DARK_GRAY + "" + i);
					List<String> lore = new ArrayList<String>();
					lore.add(ChatColor.translateAlternateColorCodes('&', "&7Click to bet on " + i + " (&0BLACK&7)"));
					lore.add(ChatColor.translateAlternateColorCodes('&', "&eBet Win: &636x"));
					if (!rolling) {
						lore.add(ChatColor.translateAlternateColorCodes('&', "&eRolling in &a" + diff + " Seconds"));
					} else {
						lore.add(ChatColor.translateAlternateColorCodes('&', "&3&lROLLING"));
					}
					im.setLore(lore);

					PersistentDataContainer container = im.getPersistentDataContainer();
					container.set(roulette.blackKey, PersistentDataType.INTEGER, i);
					container.set(roulette.rouletteKey, PersistentDataType.INTEGER, 1);
					container.set(roulette.minbetKey, PersistentDataType.DOUBLE, minbet);
					container.set(roulette.maxbetKey, PersistentDataType.DOUBLE, maxbet);
					im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
					is.setItemMeta(im);
					table.setItem(i - 1, is);
				}
			}
		}
		Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
			public void run() {
				updateTable();
			}
		}, 10);
	}

	boolean canRollTable = true;

	void rollTable(int slot) {
		canRollTable = false;
		long now = Calendar.getInstance().getTimeInMillis();
		if (stopRolling <= now) {
			// util.debug("Clear bets and reward winners.");
			rolling = false;
			rollTime = now + (interval * 1000);
			boolean red = false;
			boolean black = false;
			boolean green = false;
			int n = 0;
			ItemStack is = null;
			if (slot == 0) {
				green = true;
			} else {
				is = table.getItem(slot - 1);
			}

			if (!green) {
				ItemMeta im = is.getItemMeta();
				PersistentDataContainer container = im.getPersistentDataContainer();
				if (container.has(roulette.redKey, PersistentDataType.INTEGER)) {
					red = true;
					n = container.get(roulette.redKey, PersistentDataType.INTEGER);
				} else if (container.has(roulette.blackKey, PersistentDataType.INTEGER)) {
					black = true;
					n = container.get(roulette.blackKey, PersistentDataType.INTEGER);
				}
			}

			for (HumanEntity ent : table.getViewers()) {
				Player p = (Player) ent;
				PlayerData data = main.getPlayers().getPlayerData(p);
				List<String> bets = getPlacedBets(p);
				boolean won = false;
				for (String s : bets) {
					String[] parts = s.split(" ");
					String type = parts[0];
					double bet = Double.parseDouble(parts[1]);

					if (type.equalsIgnoreCase("red") && red) {
						double win = bet * 2;
						data.addMoney(win);
						util.message(p, "&eYou've won &6$" + win + "&e from the &cRoulette");
						if (!won) {
							won = true;
						}
					} else if (type.equalsIgnoreCase("black") && black) {
						double win = bet * 2;
						data.addMoney(win);
						util.message(p, "&eYou've won &6$" + win + "&e from the &cRoulette");
						if (!won) {
							won = true;
						}
					} else if (type.equalsIgnoreCase("0") && green) {
						double win = bet * 36;
						data.addMoney(win);
						util.message(p, "&eYou've won &6$" + win + "&e from the &cRoulette");
						if (!won) {
							won = true;
						}
					} else if (type.equalsIgnoreCase(n + "")) {
						double win = bet * 36;
						data.addMoney(win);
						util.message(p, "&eYou've won &6$" + win + "&e from the &cRoulette");
						if (!won) {
							won = true;
						}
					}
				}
				if (!won) {
					p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1f, 1f);
				} else {
					p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_LAND, 1f, 1f);
				}
			}
			placedBets.clear();
			canRollTable = true;
		} else {
			if (slot < 0) {
				// do nothing
			} else if (slot == 0) {
				for (int s = 35; s < 45; s++) {
					ItemStack is = table.getItem(s);
					is.removeEnchantment(Enchantment.SILK_TOUCH);
					table.setItem(s, is);
				}
			} else {
				ItemStack is = table.getItem(slot - 1);
				is.removeEnchantment(Enchantment.SILK_TOUCH);
				table.setItem(slot - 1, is);
			}

			Random random = new Random();

			int rand = random.nextInt(37);
			if (rand == 0) {
				for (int s = 35; s < 45; s++) {
					ItemStack is = table.getItem(s);
					is.addUnsafeEnchantment(Enchantment.SILK_TOUCH, 1);
					table.setItem(s, is);
				}
			} else {
				ItemStack is = table.getItem(rand - 1);
				is.addUnsafeEnchantment(Enchantment.SILK_TOUCH, 1);
				table.setItem(rand - 1, is);
			}

			for (HumanEntity ent : table.getViewers()) {
				Player p = (Player) ent;
				p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASEDRUM, 1f, 1f);
			}

			Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
				public void run() {
					canRollTable = true;
					rollTable(rand);
				}
			}, speed);
		}
	}

	public Inventory getTable() {
		return table;
	}

	public double getSelectedBet(Player player) {
		return getSelectedBet(player.getName());
	}

	public double getSelectedBet(String name) {
		if (selectedBets.containsKey(name)) {
			return selectedBets.get(name);
		} else {
			return 0;
		}
	}

	public double setSelectedBet(Player player, double bet) {
		return setSelectedBet(player.getName(), bet);

	}

	public double setSelectedBet(String name, double bet) {
		if (bet < 0) {
			bet = 0;
		}
		bet = Math.round(bet * 100.0) / 100.0;
		selectedBets.put(name, bet);
		return bet;
	}

	public double raiseSelectedBet(String name, double bet) {
		return setSelectedBet(name, getSelectedBet(name) + bet);
	}

	public double lowerSelectedBet(String name, double bet) {
		return setSelectedBet(name, getSelectedBet(name) - bet);
	}

	public double raiseSelectedBet(Player player, double bet) {
		return raiseSelectedBet(player.getName(), bet);
	}

	public double lowerSelectedBet(Player player, double bet) {
		return lowerSelectedBet(player.getName(), bet);
	}

	public List<String> getBetTypes() {
		return bettypes;
	}

	public List<String> getBetList(String name) {
		if (placedBets.containsKey(name)) {
			return placedBets.get(name);
		} else {
			return null;
		}
	}

	public boolean isRolling() {
		return rolling;
	}

	public void placeBet(Player player, String type) {
		double bet = getSelectedBet(player);
		if (bet > 0) {
			PlayerData data = main.getPlayers().getPlayerData(player);
			if (data.getMoney() >= bet) {
				data.takeMoney(bet);
				List<String> bets = getPlacedBets(player);
				bets.add(type + " " + bet);
				setPlacedBets(player, bets);
				util.message(player, "&aYou placed a bet on " + type + " for &6$" + bet);
			} else {
				util.error(player, "Insufficient Money Balance for that bet. Current balance: " + data.getMoney());
			}
		} else {
			util.error(player, "Can't bet $0");
		}
	}

	public List<String> getPlacedBets(Player player) {
		if (placedBets.containsKey(player.getName())) {
			return placedBets.get(player.getName());
		} else {
			return new ArrayList<String>();
		}
	}

	public void setPlacedBets(Player player, List<String> list) {
		placedBets.put(player.getName(), list);
	}
}
