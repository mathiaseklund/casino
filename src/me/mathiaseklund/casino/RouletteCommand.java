package me.mathiaseklund.casino;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RouletteCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					if (main.getCfg().getRoulette()) {
						main.getRoulette().openRouletteTable(player, "general");
					} else {
						util.error(sender, "Roulette is temporarily disabled.");
					}
				}
			}
		});
		return false;
	}

}
